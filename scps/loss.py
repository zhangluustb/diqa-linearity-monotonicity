import warnings

with warnings.catch_warnings():
    from keras import losses
    from keras import backend as K
    import tensorflow as tf


#def ranking_loss(pairwise_weight=0.1, pearson_weight=0.1):
def ranking_loss(pairwise_weight=0.1):
    def _pearson_loss(y_true, y_pred):
        d_true = y_true - tf.reduce_mean(y_true, 0)
        d_pred = y_pred - tf.reduce_mean(y_pred, 0)

        s_true = tf.reduce_sum(tf.square(d_true), 0)
        s_pred = tf.reduce_sum(tf.square(d_pred), 0)

        num = tf.sqrt(tf.multiply(s_true, s_pred))
        den = tf.reduce_sum(tf.multiply(d_pred, d_true), 0)

        return tf.divide(num, den)

    def _pairwise_loss(y_true, y_pred):
        d_true = tf.squared_difference(y_true, tf.transpose(y_true))
        d_true = tf.sqrt(tf.cast(d_true, tf.float32))

        # adding a small constant to tensor to avoid all zeros tensor (which makes the derivative to NaN)
        d_pred = tf.squared_difference(y_pred, tf.transpose(y_pred))
        d_pred = tf.sqrt(tf.cast(d_pred, tf.float32) + 1e-10)

        return tf.expand_dims(
            tf.reduce_mean(tf.reduce_sum(tf.square(d_true - d_pred), 0), 0), -1
        )

    def loss_function(y_true, y_pred):
        #mse_loss = losses.mean_squared_error(y_true, y_pred)
        pairwise_loss = _pairwise_loss(y_true, y_pred)
        pearson_loss = _pearson_loss(y_true, y_pred)

        return (
            #pairwise_weight * pairwise_loss + pearson_weight * pearson_loss + mse_loss
            pairwise_weight * pairwise_loss + pearson_loss
        )

    return loss_function
